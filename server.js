var express = require('express');
var usermodel = require('./database/model/model.js');
var server = express();
var main = express();
var bodyParser = require('body-parser');
var jwt = require('jsonwebtoken');
var secservice = require('./services/security.js')
const bearerToken = require('express-bearer-token');


var signkey='aspekey';


server.get('/', function (req, res) {
  var user= {firstName:req.query.user,password:req.query.password};
  console.log(usermodel);
  usermodel.findOne({ where: {firstName: user.firstName} }).then(function(founduser){
      console.log('founduser',founduser.firstName,'user',user)
      if(founduser.firstName ==user.firstName && founduser.password==user.password){
        token=jwt.sign({type:'professore',permissions:['criar questoes','explodir tudo']},signkey);
        res.send(token);
      }
      else {
        res.status(403).send("Not authorized")
      }
  });

//  res.send('Hello World!');
});


server.post('/authentication', function (req,res){
  ss=new secservice();

  var user=req.body;

  //console.log(ss.getToken('1000','admin'));
  var p = ss.getToken(user.user,user.pass);
  p.then(function(result){res.send(result)})
  .catch(function(){res.status(401).send("Authentication failed");});
  //console.log(req.body);
   //res.send(req.body);
});

server.get('/authentication',function(req,res){
  ss=new secservice();
  try {
    ss.validateToken(req.token);
    res.send("ok");
  }catch(err){
    //console.log(err);
    res.status(401).send("Invalid Token");
  }
});

server.get('/test', function (req,res){
  newperfil={descricao:"novo aluno"};
  newpermissao={descricao:"cagar regra"};
  var np;
  usermodel.permissao.create(newpermissao).then(function(novapermissao){
    console.log('created a permission',novapermissao);

    np= novapermissao;
  });

  usermodel.perfil.create(newperfil).then(function(created){
      console.log("show the ids",created.id,np.id)
      usermodel.acesso.create({perfil_id:created.id,permissao_id:np.id});
  });
  //console.log(usermodel.usuario);
  //res.send(usermodel.usuario);
});

server.use(bodyParser.json());
main.use(bodyParser.json());
main.use(bearerToken());
main.use('/api/security',server);
//console.log('usermodel',usermodel,'server',server);

module.exports=main;
