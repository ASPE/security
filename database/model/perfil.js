var Sequelize = require('sequelize')
var sequelize = require('../connection.js')
var usuario = require ('./usuario.js')
var permissao = require ('./permissao.js')

var Perfil = sequelize.define('perfil', {
  tipo: {
    type: Sequelize.STRING
  },
  descricao: {
    type: Sequelize.STRING
  },
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});


usuario.belongsTo(Perfil);
//Perfil.hasMany(permissao,{as : 'permissions'})
//permissao.belongsToMany(Perfil, {through: 'PerfilAssociado'})
module.exports=Perfil;
