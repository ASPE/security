var Sequelize = require('sequelize')
var sequelize = require('../connection.js')


var Permissao = sequelize.define('permissao', {
  descricao: {
    type: Sequelize.STRING
  },
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});

module.exports=Permissao;
