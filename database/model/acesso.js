var perfil = require ('./perfil.js');
var permissao = require ('./permissao.js');
var sequelize = require ('../connection.js');
var Sequelize = require ('sequelize');


var Acesso = sequelize.define('acesso', {
  perfil_id: {
    type: Sequelize.STRING
  },
  permissao_id: {
    type: Sequelize.STRING
  }
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});

permissao.belongsToMany(perfil, {
  through:{
    model:Acesso,
    unique:false
  },
  foreignKey: 'permissao_id',
  constraints: false
});

perfil.belongsToMany(permissao,{
  through:{
    model: Acesso,
    unique:false
  },
  foreignKey:'perfil_id',
});

module.exports.acesso=Acesso;
module.exports.perfil=perfil;
module.exports.permissao=permissao;
