var Sequelize = require('sequelize')
var sequelize = require('../connection.js')


var User = sequelize.define('usuario', {
  matricula: {
    type: Sequelize.STRING
  },
  nome: {
    type: Sequelize.STRING
  },
  senha: {
    type: Sequelize.STRING
  }
}, {
  freezeTableName: true // Model tableName will be the same as the model name
});



module.exports=User;
