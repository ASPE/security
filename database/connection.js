var Sequelize = require('sequelize');

var sequelize = new Sequelize('security', 'root', '123456', {
  host: 'localhost',
  dialect: 'mysql',

  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
});

// Or you can simply use a connection uri
module.exports = sequelize;
