var server=require('./server.js');

function RESTapi(port,server){
  this.port=port;
  this.server=server;
}


RESTapi.prototype.listen= function(){
  server.listen(this.port);
  console.log('server listening on',this.port);
}


var rest=new RESTapi(30002,server);
rest.listen();

// sign with RSA SHA256
//var cert = fs.readFileSync('private.key');  // get private key
//var token = jwt.sign({ foo: 'bar' }, cert, { algorithm: 'RS256'});
